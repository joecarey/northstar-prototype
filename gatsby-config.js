const projectPathPrefix =
  process.env.CI_PROJECT_NAME || "northstar-prototype"

module.exports = {
  // Use CI_PROJECT_NAME variable as pathPrefix, edit/comment if you want to use a custom domain.
  // pathPrefix: `/${projectPathPrefix}`,
  pathPrefix: `/northstar-prototype`,
  plugins: [
    'gatsby-plugin-top-layout',
    'gatsby-plugin-react-helmet',
    // If you want to use styled components you should add the plugin here.
    // 'gatsby-plugin-styled-components',
    'gatsby-plugin-mui-emotion',
  ],
  siteMetadata: {
    title: `Gatsby Default Starter`,
    description: `Kick off your next, great Gatsby project with this default starter. This barebones starter ships with the main Gatsby configuration files you might need.`,
    author: `@joecarey`,
  },
};