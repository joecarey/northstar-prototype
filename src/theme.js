import { createTheme } from '@mui/material/styles';
import JSONData from "../content/theme.json"

const theme = createTheme({
    name: 'Light Theme',
    palette: {
        awesomeColors: {
            primary: '#fe6b8b',
            secondary: '#ff8e53',
        },
        primary: {
            main: JSONData.palette.primary.main,
        },
        secondary: {
            main: JSONData.palette.secondary.main,
        },
        tertiary: {
            light: JSONData.palette.tertiary.light,
            main: JSONData.palette.tertiary.main,
            dark: JSONData.palette.tertiary.dark,
            contrastText: JSONData.palette.tertiary.contrastText,
        },
        error: {
            main: JSONData.palette.error.main,
        },
        warning: {
            main: JSONData.palette.warning.main,
        },
        success: {
            main: JSONData.palette.success.main,
        },
        grey: {
            50: JSONData.palette.grey[50],
            100: JSONData.palette.grey[100],
            200: JSONData.palette.grey[200],
            300: JSONData.palette.grey[300],
            400: JSONData.palette.grey[400],
            500: JSONData.palette.grey[500],
            600: JSONData.palette.grey[600],
            700: JSONData.palette.grey[700],
            800: JSONData.palette.grey[800],
            900: JSONData.palette.grey[900],
        },
    },
    typography: {
        h1: {
            fontSize: JSONData.typography.h1.fontSize,
        },
        h2: {
            fontSize: JSONData.typography.h2.fontSize,
        },
        h3: {
            fontSize: JSONData.typography.h3.fontSize,
        },
        h4: {
            fontSize: JSONData.typography.h4.fontSize,
        },
        h5: {
            fontSize: JSONData.typography.h5.fontSize,
        },
        h6: {
            fontSize: JSONData.typography.h6.fontSize,
        },
    },
});

export default theme;