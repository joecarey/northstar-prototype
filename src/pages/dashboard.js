import * as React from 'react';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

import Copyright from '../components/Copyright';
import EChartLine from '../components/EChartLine';
import EChartPie from '../components/EChartPie';
import Deposits from '../components/Deposits';
import Orders from '../components/Orders';
import NavigationProducts from '../components/NavigationProducts';
import NavigationDrawer from '../components/NavigationDrawer';
import NavigationAppbar from '../components/NavigationAppbar';

function DashboardContent() {
    const [menuExpand, setMenuExpand] = React.useState(true);
    const [appExpand, setAppExpand] = React.useState(false);
    const [age, setAge] = React.useState(0);

    const handleChange = (event) => {
        setAge(event.target.value);
    };

    const onMenuChange = (data) => {
        setMenuExpand(data);
    };

    const onAppChange = (data) => {
        setAppExpand(data);
    };

    return (
        <>
            {/* DRAWER PRODUCT SELECT */}
            <NavigationProducts
                appExpand={appExpand}
                onAppChange={(e) => {
                    onAppChange(e);
                }}
            />

            {/* APPBAR MENU */}
            <NavigationAppbar
                // darkMode={props.darkMode}
                // onDarkModeChange={props.onDarkModeChange}
                expanded={appExpand}
                onMenuChange={(e) => {
                    onAppChange(e);
                }}
            />

            <Box sx={{ display: 'flex' }}>
                {/* DRAWER MENU */}
                <NavigationDrawer
                    menuExpand={menuExpand}
                    onMenuChange={onMenuChange}
                />
                <Box component="main"
                    sx={{
                        backgroundColor: (theme) =>
                        theme.palette.mode === 'light'
                            ? theme.palette.grey[100]
                            : theme.palette.grey[900],
                        flexGrow: 1,
                        height: '100vh',
                        overflow: 'auto',
                    }}
                >
                    <Container maxWidth="lg" sx={{ mt: 4, mb: 4, marginTop: 11 }}>
                        <Grid container spacing={3} sx={{ pb: 4 }} >
                            <Grid item xs={12}>
                                <Typography variant="overline" color="textSecondary">Overview</Typography>
                                <Typography variant="h3" component="h1" color="textPrimary">
                                    Welcome back, Joe
                                </Typography>
                                <Box component="div" sx={{ display: 'flex' }}>
                                    <Typography
                                        variant="body1"
                                        color="textSecondary"
                                        sx={{
                                            pt: 1,
                                        }}
                                    >
                                        Here's what's happening for the week of: 
                                    </Typography>
                                    <FormControl variant="standard" sx={{ p:0.5 }}>
                                        <Select
                                            labelId="demo-simple-select-standard-label"
                                            id="demo-simple-select-standard"
                                            defaultValue={0}
                                            value={age}
                                            onChange={handleChange}
                                        >
                                            <MenuItem value={0}>
                                                <Typography color="textSecondary">October 24th, 2021</Typography>
                                            </MenuItem>
                                            <MenuItem value={10}>
                                                <Typography color="textSecondary">October 17th, 2021</Typography>
                                            </MenuItem>
                                            <MenuItem value={20}>
                                                <Typography color="textSecondary">October 10th, 2021</Typography>
                                            </MenuItem>
                                            <MenuItem value={30}>
                                                <Typography color="textSecondary">October 3rd, 2021</Typography>
                                            </MenuItem>
                                        </Select>
                                    </FormControl>
                                </Box>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <Paper
                                sx={{
                                    p: 2,
                                    display: 'flex',
                                    flexDirection: 'column',
                                    height: 300,
                                }}
                                >
                                    <Typography variant="h5" component="h2">
                                        Daily usage
                                    </Typography>
                                    <EChartLine />
                                </Paper>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <Paper
                                sx={{
                                    p: 2,
                                    display: 'flex',
                                    flexDirection: 'column',
                                    height: 300,
                                }}
                                >
                                    <Typography variant="h5" component="h2">
                                        Security Exceptions
                                    </Typography>
                                    <EChartPie />
                                </Paper>
                            </Grid>
                            <Grid item xs={12} md={3}>
                                <Paper
                                    sx={{
                                        p: 2,
                                        display: 'flex',
                                        flexDirection: 'column',
                                        height: '100%',
                                    }}
                                >
                                    <Deposits />
                                </Paper>
                            </Grid>
                            <Grid item xs={9}>
                                <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
                                    <Orders />
                                </Paper>
                            </Grid>
                        </Grid>
                        <Copyright />
                    </Container>
                </Box>
            </Box>
        </>
    )
}

export default function Dashboard() {
    return <DashboardContent />;
}