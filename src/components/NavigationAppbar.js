import React from 'react';
import { styled, alpha } from '@mui/material/styles';
import PropTypes from 'prop-types';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Badge from '@mui/material/Badge';
import Link from '@mui/material/Link';
import InputBase from '@mui/material/InputBase';

// ICONS
import AppsIcon from '@mui/icons-material/Apps';
import SearchIcon from '@mui/icons-material/Search';
import CloseIcon from '@mui/icons-material/Close';
import NotificationsIcon from '@mui/icons-material/Notifications';
import PersonIcon from '@mui/icons-material/Person';

import * as styles from "./NavigationAppbar.module.css";

// import NavigationAccount from './NavigationAccount';

const Search = styled('div')(({ theme }) => ({
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    '&:hover': {
        backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(3),
        width: 'auto',
    },
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: 'inherit',
    '& .MuiInputBase-input': {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)})`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
        width: '20ch',
        },
    },
}));

function NavigationAppbar(props) {
    // const [accountEl, setAccountEl] = React.useState(null);

    const handleDrawerClick = () => {
        props.onMenuChange(!props.expanded);
    };

    // const handleAccountClick = (event) => {
    //     setAccountEl(event.currentTarget);
    // };

    return (
        <AppBar
            position="fixed"
            className={styles.appBar}
        >
            <Toolbar>
                {/* Secondary Navigation Button */}
                <IconButton
                    edge="start"
                    color="inherit"
                    aria-label="menu"
                    onClick={handleDrawerClick}
                    className={styles.menuButton}
                >
                    {props.expanded ? <CloseIcon /> : <AppsIcon />}
                </IconButton>

                {/* Product Name? */}
                <div className={styles.titleBar}>
                    <Link href="/" color="inherit">
                        {/* <HSIcon
                            hexColor="#fff"
                            width="22"
                            className={styles.logo}
                            titleAccess="HelpSystems gear icon"
                        /> */}
                        <Typography variant="h6" className={styles.title}>
                            Northstar Prototype
                        </Typography>
                    </Link>
                </div>
                
                <Search>
                    <SearchIconWrapper>
                        <SearchIcon />
                    </SearchIconWrapper>
                    <StyledInputBase
                        placeholder="Search…"
                        inputProps={{ 'aria-label': 'search' }}
                    />
                </Search>
                
                {/* <Menu Icons /> */}
                <div className={styles.toolbarIcon}>
                    {/* Notifications Icon */}
                    <IconButton color="inherit" aria-label="notification">
                        <Badge badgeContent={3} color="error">
                            <NotificationsIcon />
                        </Badge>
                    </IconButton>
                    {/* Account Icon - Dropdown */}
                    <IconButton
                        aria-label="account"
                        aria-controls="account-select"
                        aria-haspopup="true"
                        color="inherit"
                        // onClick={handleAccountClick}
                    >
                        <PersonIcon />
                    </IconButton>
                    {/* <NavigationAccount
                        darkMode={props.darkMode}
                        onDarkModeChange={props.onDarkModeChange}
                        accountEl={accountEl}
                        setAccountEl={setAccountEl}
                    /> */}
                </div>
            </Toolbar>
        </AppBar>
    );
}

NavigationAppbar.propTypes = {
    darkMode: PropTypes.bool,
    expanded: PropTypes.bool,
    onMenuChange: PropTypes.func,
    onDarkModeChange: PropTypes.func,
};

NavigationAppbar.defaultProps = {
    darkMode: false,
    expanded: false,
    onMenuChange: () => {},
    onDarkModeChange: () => {},
};

export default NavigationAppbar;
