import React from 'react';
import * as echarts from 'echarts';
import ReactECharts from 'echarts-for-react';
import echartsTheme from '../echartsTheme';

echarts.registerTheme('theme', echartsTheme);

export default function EChartPie() {
    const options = {
        tooltip: {
            trigger: 'item',
        },
        legend: {
            orient: 'vertical',
            left: 'left',
        },
        series: [
            {
                name: 'Types of Charts',
                type: 'pie',
                radius: '50%',
                data: [
                    { value: 1048, name: 'Bar' },
                    { value: 735, name: 'Line' },
                    { value: 580, name: 'Pie' },
                    { value: 484, name: 'Scatter' },
                    { value: 300, name: 'Gauge' },
                ],
                emphasis: {
                    itemStyle: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)',
                    },
                },
            },
        ],
    };

    return (
        <ReactECharts
            option={options}
            theme="theme"
            style={{ width: '600px', maxWidth: '100%', marginTop: '20px' }}
        />
    );
}