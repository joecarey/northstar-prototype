import React from 'react';
import PropTypes from 'prop-types';
import Drawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import CloseIcon from '@mui/icons-material/Close';

import * as styles from "./NavigationProducts.module.css";

function NavigationProducts(props) {
    const toggleDrawer = (event) => {
        if (
            event.type === 'keydown' &&
            (event.key === 'Tab' || event.key === 'Shift')
        ) {
            return;
        }
        props.onAppChange(!props.appExpand);
    };

    const list = () => (
        <div
            className={styles.list}
            role="presentation"
            onClick={toggleDrawer}
            onKeyDown={toggleDrawer}
        >
            <IconButton
                className={styles.closeBtn}
                color="inherit"
                aria-label="Close"
            >
                <CloseIcon color="action" />
            </IconButton>
            {/* HSOne */}
            <List>
                {['HelpSystems One'].map((text) => (
                    <ListItem 
                        className={styles.listItem} 
                        button 
                        key={text}
                    >
                        <ListItemText primary={text} />
                    </ListItem>
                ))}
            </List>
            <Divider />
            {/* Security */}
            <Typography
                variant="subtitle2"
                color="textSecondary"
                className={styles.menuTitle}
            >
                Security
            </Typography>
            <List>
                {[
                    'Clearswift SEG',
                    'Clearswift SIG',
                    'GoAnywhere MFT',
                    'Powertech Antivirus',
                ].map((text) => (
                    <ListItem 
                        className={styles.listItem}
                        button 
                        key={text}
                    >
                        <ListItemText primary={text} />
                    </ListItem>
                ))}
            </List>
            <Divider />
            {/* Automation */}
            <Typography
                variant="subtitle2"
                color="textSecondary"
                className={styles.menuTitle}
            >
                Automation
            </Typography>
            <List>
                {['Automate', 'JAMS'].map((text) => (
                    <ListItem 
                        className={styles.listItem}
                        button
                        key={text}
                    >
                        <ListItemText primary={text} />
                    </ListItem>
                ))}
            </List>
        </div>
    );

    return (
        <Drawer 
            open={props.appExpand} 
            onClose={toggleDrawer}
            className={styles.appDrawer}
        >
            {list()}
        </Drawer>
    );
}

NavigationProducts.propTypes = {
    appExpand: PropTypes.bool,
    onAppChange: PropTypes.func,
};

NavigationProducts.defaultProps = {
    appExpand: false,
    onAppChange: () => {},
};

export default NavigationProducts;
