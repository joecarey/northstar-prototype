import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Drawer from '@mui/material/Drawer';
import Menu from '@mui/material/Menu';
import MenuList from '@mui/material/MenuList';
import MenuItem from '@mui/material/MenuItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import Collapse from '@mui/material/Collapse';
import Divider from '@mui/material/Divider';
// ICONS
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import MenuIcon from '@mui/icons-material/Menu';
import MenuOpenIcon from '@mui/icons-material/MenuOpen';
import MoreVertIcon from '@mui/icons-material/MoreVert';

import * as styles from "./NavigationDrawer.module.css";

function NavigationDrawer(props) {
    const [nestOpen, setNestOpen] = React.useState(false);
    const [workflowOpen, setWorkflowOpen] = React.useState(false);
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleNestClick = () => {
        setNestOpen(!nestOpen);
    };
    const handleWorkflowClick = () => {
        setWorkflowOpen(!workflowOpen);
    };
    const handleIconClick = () => {
        props.onMenuChange(!props.menuExpand);
    };

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    const leftSideMenu = () => (
        <MenuList>
            <MenuItem className={styles.menuItem}>Overview</MenuItem>
            <MenuItem className={styles.menuItem} onClick={handleNestClick}>
                Dashboards
                {nestOpen ? (
                    <ExpandLess className={styles.expandIcon} />
                ) : (
                    <ExpandMore className={styles.expandIcon} />
                )}
            </MenuItem>
            <Collapse in={nestOpen} timeout="auto" unmountOnExit>
                <MenuList className={styles.menuListNested}>
                    {['Security Dashboard', 'Automation Dashboard', 'SEG Dashboard', 'All Dashboards'].map(
                        (text, index) => {
                            return (
                                <MenuItem
                                    key={`${text + index}`}
                                    className={styles.menuItem}
                                >
                                    {text}
                                </MenuItem>
                            );
                        }
                    )}
                </MenuList>
            </Collapse>
            <MenuItem className={styles.menuItem}>Reporting</MenuItem>
            <MenuItem className={styles.menuItem} onClick={handleWorkflowClick}>
                Workflows
                {workflowOpen ? (
                    <ExpandLess className={styles.expandIcon} />
                ) : (
                    <ExpandMore className={styles.expandIcon} />
                )}
            </MenuItem>
            <Collapse in={workflowOpen} timeout="auto" unmountOnExit>
                <MenuList className={styles.menuListNested}>
                    {['Overview', 'Discover', 'History', 'All Workflows'].map(
                        (text, index) => {
                            return (
                                <MenuItem
                                    key={`${text + index}`}
                                    className={styles.menuItem}
                                >
                                    {text}
                                </MenuItem>
                            );
                        }
                    )}
                </MenuList>
            </Collapse>
            <MenuItem className={styles.menuItem}>Marketplace</MenuItem>
        </MenuList>
    );

    return (
        <Drawer
            variant="permanent"
            open={props.menuExpand}
            className={classNames(styles.drawer, {
                [styles.drawerOpen]: props.menuExpand,
                [styles.drawerClose]: !props.menuExpand,
            })}
            classes={{
                paper: props.menuExpand
                    ? styles.drawerOpen
                    : styles.drawerClose,
            }}
        >
            {/* LIST */}
            <MenuList className={styles.menu}>
                <MenuItem
                    className={styles.menuToggle}
                    onClick={handleIconClick}
                >
                    <ListItemIcon className={styles.toggleIcon}>
                        {props.menuExpand ? (
                            <MenuOpenIcon color="action" />
                        ) : (
                            <MenuIcon color="action" />
                        )}
                    </ListItemIcon>
                </MenuItem>
            </MenuList>
            <Divider />
            {props.menuExpand ? (
                leftSideMenu()
            ) : (
                <>
                    <MenuList className={styles.menu}>
                        <MenuItem
                            className={styles.menuToggle}
                            onClick={handleClick}
                        >
                            <ListItemIcon className={styles.toggleIcon}>
                                <MoreVertIcon />
                            </ListItemIcon>
                        </MenuItem>
                    </MenuList>
                    <Menu
                        id="left-side-menu"
                        anchorEl={anchorEl}
                        open={Boolean(anchorEl)}
                        onClose={handleClose}
                    >
                        {leftSideMenu()}
                    </Menu>
                </>
            )}
        </Drawer>
    );
}

NavigationDrawer.propTypes = {
    menuExpand: PropTypes.bool,
    onMenuChange: PropTypes.func,
};

NavigationDrawer.defaultProps = {
    menuExpand: false,
    onMenuChange: () => {},
};

export default NavigationDrawer;
